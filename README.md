# OpenML dataset: Airline_delay

https://www.openml.org/d/41088

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**: Gaussian Processes for Big Data (In UAI'13). J. Hensman, N. Fusi and N. D.Lawrence  

Airline delay data used for demonstrating Gaussian processes for big data.Flight arrival and departure times for every commercial flight in the USA from
January 2008 to April 2008.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41088) of an [OpenML dataset](https://www.openml.org/d/41088). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41088/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41088/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41088/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

